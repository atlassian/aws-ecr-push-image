FROM python:3.10-slim

RUN apt-get update \
    && apt-get install --no-install-recommends -y docker=1.5-2 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*


# install requirements
COPY requirements.txt /
WORKDIR /
RUN pip install --no-cache-dir -r requirements.txt

# copy the pipe source code
COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["python3", "/pipe.py"]
