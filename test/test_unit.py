import configparser
import io
import os
import sys
import shutil
from contextlib import contextmanager
from copy import copy, deepcopy
from unittest import TestCase

import pytest

from pipe.pipe import ECRPush, schema


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class CloudformationDeployTestCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, mocker):
        self.mocker = mocker

    def setUp(self):
        self.schema = deepcopy(schema)
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path
        test_dir = os.path.join(os.environ["HOME"], '.aws')
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)

    def test_discover_auth_method_default_credentials_only(self):
        self.mocker.patch.dict(
            os.environ, {
                'AWS_ACCESS_KEY_ID': 'akiafkae',
                'AWS_SECRET_ACCESS_KEY': 'secretkey',
                'AWS_DEFAULT_REGION': 'test-region',
                'IMAGE_NAME': 'test-image',
                'AWS_OIDC_ROLE_ARN': ''
            }
        )
        cloudformation_pipe = ECRPush(schema=self.schema, check_for_newer_version=True)
        cloudformation_pipe.auth()
        self.assertEqual(cloudformation_pipe.auth_method, 'DEFAULT_AUTH')
        base_config_dir = f'{os.environ["HOME"]}/.aws'

        self.assertFalse(os.path.exists(f'{base_config_dir}/.aws-oidc'))

    def test_discover_auth_method_oidc_only(self):
        self.mocker.patch.dict(
            os.environ, {
                'BITBUCKET_STEP_OIDC_TOKEN': 'token',
                'AWS_OIDC_ROLE_ARN': 'account/role',
                'AWS_DEFAULT_REGION': 'test-region',
                'IMAGE_NAME': 'test-image'
            }
        )
        cloudformation_pipe = ECRPush(schema=self.schema, check_for_newer_version=True)
        cloudformation_pipe.auth()
        self.assertEqual(cloudformation_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

    def test_discover_auth_method_oidc_and_default_credentials(self):
        self.mocker.patch.dict(
            os.environ, {
                'BITBUCKET_STEP_OIDC_TOKEN': 'token',
                'AWS_OIDC_ROLE_ARN': 'account/role',
                'AWS_ACCESS_KEY_ID': 'akiafkae',
                'AWS_SECRET_ACCESS_KEY': 'secretkey',
                'AWS_DEFAULT_REGION': 'test-region',
                'IMAGE_NAME': 'test-image'
            }
        )
        self.assertTrue('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertTrue('AWS_SECRET_ACCESS_KEY' in os.environ)

        cloudformation_pipe = ECRPush(schema=self.schema, check_for_newer_version=True)
        cloudformation_pipe.auth()

        self.assertEqual(cloudformation_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

        self.assertFalse('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertFalse('AWS_SECRET_ACCESS_KEY' in os.environ)

    def test_ecr_public_success(self):
        self.mocker.patch.dict(
            os.environ, {
                'AWS_ACCESS_KEY_ID': 'akiafkae',
                'AWS_SECRET_ACCESS_KEY': 'secretkey',
                'IMAGE_NAME': 'test-image',
                'ECR_PUBLIC': 'true',
                'PUBLIC_REGISTRY_ALIAS': 'test-alias'
            }
        )
        pipe = ECRPush(
            schema=self.schema, check_for_newer_version=True)

        resolve_ecr_public = pipe.resolve_ecr_public()

        self.assertTrue(resolve_ecr_public)

    def test_ecr_public_non_boolean_fails(self):
        self.mocker.patch.dict(
            os.environ, {
                'AWS_ACCESS_KEY_ID': 'akiafkae',
                'AWS_SECRET_ACCESS_KEY': 'secretkey',
                'AWS_DEFAULT_REGION': 'test-region',
                'IMAGE_NAME': 'test-image',
                'ECR_PUBLIC': 'non-boolean',
                'PUBLIC_REGISTRY_ALIAS': 'test-alias'
            }
        )

        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:

                ECRPush(schema=self.schema, check_for_newer_version=True)

        assert "ECR_PUBLIC:\n- must be of boolean type" in out.getvalue()
        assert pytest_wrapped_e.type is SystemExit
