import os
from uuid import uuid4

import boto3

from bitbucket_pipes_toolkit.test import PipeTestCase


class ECRTestCase(PipeTestCase):

    maxDiff = None

    def setUp(self):
        super().setUp()
        self.base_name = "test/bbci-pipes-test-ecr"
        self.repository_name = f"{self.base_name}-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
        self.image_name = self.repository_name
        self.tag = uuid4().hex
        self.image, _ = self.docker_client.images.build(
            path=os.path.join(os.path.dirname(__file__), 'context'), tag=self.image_name)
        self.image.tag(self.image_name, self.tag)

        self.public_base_name = "bbci-pipes-test-ecr-public"
        self.public_repository_name = f"{self.public_base_name}-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
        self.public_registry_alias = os.getenv('REPOSITORY_URI').removeprefix('public.ecr.aws').removesuffix(self.public_repository_name).strip('/')
        self.public_image_name = self.public_repository_name
        self.public_tag = uuid4().hex
        self.public_image, _ = self.docker_client.images.build(
            path=os.path.join(os.path.dirname(__file__), 'context'), tag=f"{self.public_registry_alias}/{self.public_image_name}")
        self.public_image.tag(f"{self.public_registry_alias}/{self.public_image_name}", self.public_tag)

    def test_default_success(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": self.image_name,
            "REPOSITORY": self.repository_name,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Successfully pushed', result)

    def test_default_ecr_public_success(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "ECR_PUBLIC": "true",
            "PUBLIC_REGISTRY_ALIAS": self.public_registry_alias,
            "IMAGE_NAME": self.public_image_name,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Successfully pushed', result)

    def test_role_arn_success(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "AWS_ROLE_ARN": os.getenv("AWS_ROLE_ARN"),
            "AWS_ROLE_SESSION_NAME": os.getenv("AWS_ROLE_SESSION_NAME"),
            "IMAGE_NAME": self.image_name,
            "REPOSITORY": self.repository_name,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Successfully pushed', result)

    def test_image_is_available_in_ecr(self):
        client = boto3.client('ecr', region_name=os.getenv('AWS_DEFAULT_REGION'))

        tags = f"{self.tag} latest"

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": self.image_name,
            "TAGS": tags,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Successfully pushed', result)

        images = client.list_images(repositoryName=self.repository_name)
        ecr_tags = [image.get('imageTag') for image in images['imageIds']]
        self.assertIn(tags.split()[0], ecr_tags)
        self.assertIn(tags.split()[1], ecr_tags)

    def test_missing_variables(self):
        result = self.run_container(environment={
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('AWS_ACCESS_KEY_ID:\n- required field', result)

    def test_image_build_fails_image_doesnt_exist(self):
        invalid_image = "no-such-image"
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": invalid_image,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn(f'Image not found error: {invalid_image}:latest.', result)

    def test_image_push_fails_image_not_built_with_provided_tag(self):
        tag_without_image_built = 'image-not-built-tag'
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": self.image_name,
            "TAGS": tag_without_image_built,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn(f'Image not found error: {self.image_name}:{tag_without_image_built}', result)

    def test_debug_messages_are_in_logs(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": self.image_name,
            "DEBUG": 'true',

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('DEBUG:', result)


class ECRMissingRepositoryTestCase(PipeTestCase):

    maxDiff = None

    def setUp(self):
        self.image_name = 'no-such-repository'
        self.tag = uuid4().hex
        super().setUp()
        self.image, _ = self.docker_client.images.build(
            path=os.path.join(os.path.dirname(__file__), 'context'), tag=self.image_name)
        self.image.tag(self.image_name, self.tag)

    def test_image_push_fails_repository_doesnt_exist(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": self.image_name,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Docker push error', result)


class ECRAuthODCTestCase(PipeTestCase):

    maxDiff = None

    def setUp(self):
        super().setUp()
        self.base_name = "test/bbci-pipes-test-ecr"
        self.repository_name = f"{self.base_name}-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
        self.image_name = self.repository_name
        self.tag = uuid4().hex
        self.image, _ = self.docker_client.images.build(
            path=os.path.join(os.path.dirname(__file__), 'context'), tag=self.image_name)
        self.image.tag(self.image_name, self.tag)

    def test_default_success(self):
        result = self.run_container(environment={
            "AWS_OIDC_ROLE_ARN": os.getenv("AWS_OIDC_ROLE_ARN"),
            "BITBUCKET_STEP_OIDC_TOKEN": os.getenv("BITBUCKET_STEP_OIDC_TOKEN"),

            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": self.image_name,
            "REPOSITORY": self.repository_name,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Authenticating with a OpenID Connect (OIDC) Web Identity Provider', result)
        self.assertIn('Successfully pushed', result)
