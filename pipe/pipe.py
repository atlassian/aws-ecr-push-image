import os
import time
import stat
import base64
import configparser

import yaml
import docker
import docker.errors
import boto3
from botocore.exceptions import ClientError

from bitbucket_pipes_toolkit import Pipe, get_logger


DEFAULT_REGION = "us-east-1"

logger = get_logger()

schema = {
    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': True},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': True},
    'AWS_DEFAULT_REGION': {'type': 'string', 'required': True},
    'IMAGE_NAME': {'type': 'string', 'required': True},
    'TAGS': {'type': 'string', 'required': False, 'nullable': True, 'default': 'latest'},
    'AWS_ROLE_ARN': {'type': 'string', 'required': False},
    'AWS_ROLE_SESSION_NAME': {'type': 'string', 'required': False},
    'ECR_PUBLIC': {'type': 'boolean', 'required': False, 'default': False},
    'PUBLIC_REGISTRY_ALIAS': {'type': 'string', 'required': False},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}

}


class ECRPush(Pipe):
    OIDC_AUTH = 'OIDC_AUTH'
    DEFAULT_AUTH = 'DEFAULT_AUTH'

    def __init__(self, schema, *args, **kwargs):
        self.schema = schema
        self.auth_method = self.discover_auth_method()
        self.ecr_public_check = self.resolve_ecr_public()
        self.role_arn_check = self.resolve_role_arn()
        super().__init__(schema=self.schema, *args, **kwargs)
        # Set up and boto3_client and region depending on pushing
        # ecr or ecr-public registries.
        if self.ecr_public_check:
            self.boto3_client_name = 'ecr-public'
            self.region = DEFAULT_REGION
            self.public_registry = 'public.ecr.aws'
            self.public_registry_alias = self.get_variable('PUBLIC_REGISTRY_ALIAS')
        else:
            self.boto3_client_name = 'ecr'
            self.region = self.get_variable('AWS_DEFAULT_REGION')

        self.docker_client = None

    def resolve_ecr_public(self):
        ecr_public = yaml.safe_load(os.getenv('ECR_PUBLIC', 'False'))
        if ecr_public is True:
            self.schema['AWS_DEFAULT_REGION']['required'] = False
            self.schema['PUBLIC_REGISTRY_ALIAS']['required'] = True

            return True

        return False

    def discover_auth_method(self):
        """Discover user intentions: authenticate to AWS through OIDC or default aws access keys"""
        oidc_role = os.getenv('AWS_OIDC_ROLE_ARN')
        web_identity_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        if oidc_role:
            if web_identity_token:
                logger.info('Authenticating with a OpenID Connect (OIDC) Web Identity Provider')
                self.schema['BITBUCKET_STEP_OIDC_TOKEN'] = {'required': True}
                self.schema['AWS_OIDC_ROLE_ARN'] = {'required': True}

                self.schema['AWS_ACCESS_KEY_ID']['required'] = False
                self.schema['AWS_SECRET_ACCESS_KEY']['required'] = False

                # unset env variables to prevent aws client general auth
                # https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html
                if 'AWS_ACCESS_KEY_ID' in os.environ:
                    del os.environ['AWS_ACCESS_KEY_ID']
                if 'AWS_SECRET_ACCESS_KEY' in os.environ:
                    del os.environ['AWS_SECRET_ACCESS_KEY']
                if 'AWS_SESSION_TOKEN' in os.environ:
                    del os.environ['AWS_SESSION_TOKEN']

                return self.OIDC_AUTH

            logger.warning('Parameter "oidc: true" in the step configuration is required for OIDC authentication')
            logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY')
            return self.DEFAULT_AUTH

        logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY')
        return self.DEFAULT_AUTH

    def resolve_role_arn(self):
        if os.getenv('AWS_ROLE_ARN'):
            self.schema['AWS_ROLE_ARN']['required'] = True
            self.schema['AWS_ROLE_SESSION_NAME']['required'] = True

            return True

        return False

    def auth(self):
        """Authenticate via chosen method"""
        if self.auth_method == self.OIDC_AUTH:
            random_number = str(time.time_ns())
            aws_config_directory = os.path.join(os.environ["HOME"], '.aws')
            oidc_token_directory = os.path.join(aws_config_directory, '.aws-oidc')

            os.makedirs(aws_config_directory, exist_ok=True)
            os.makedirs(oidc_token_directory, exist_ok=True)

            web_identity_token_path = os.path.join(oidc_token_directory, f'oidc_token_{random_number}')
            with open(web_identity_token_path, 'w') as f:
                f.write(self.get_variable('BITBUCKET_STEP_OIDC_TOKEN'))

            os.chmod(web_identity_token_path, mode=stat.S_IRUSR)
            logger.debug('Web identity token file is created')

            aws_configfile_path = os.path.join(aws_config_directory, 'config')
            with open(aws_configfile_path, 'w') as configfile:
                config = configparser.ConfigParser()
                config['default'] = {
                    'role_arn': self.get_variable('AWS_OIDC_ROLE_ARN'),
                    'web_identity_token_file': web_identity_token_path
                }
                config.write(configfile)
            logger.debug('Configured settings for authentication with assume web identity role')

    def get_client(self):
        try:
            if self.role_arn_check:
                role_arn = self.get_variable('AWS_ROLE_ARN')
                role_session_name = self.get_variable('AWS_ROLE_SESSION_NAME')
                logger.info('Using STS assume role with AWS_ROLE_ARN')

                session = self.role_arn_to_session(role_arn, role_session_name)

                return session.client(self.boto3_client_name, region_name=self.region)

            return boto3.client(self.boto3_client_name, region_name=self.region)
        except ClientError as err:
            self.fail(f"Failed to create boto3 client.\n Error: {str(err)}")

    @staticmethod
    def role_arn_to_session(role_arn, role_session_name):
        client = boto3.client('sts')
        response = client.assume_role(
            RoleArn=role_arn,
            RoleSessionName=role_session_name
        )

        return boto3.Session(
            aws_access_key_id=response['Credentials']['AccessKeyId'],
            aws_secret_access_key=response['Credentials']['SecretAccessKey'],
            aws_session_token=response['Credentials']['SessionToken'])

    def get_docker_client(self):
        if self.docker_client is None:
            self.docker_client = docker.from_env()
        return self.docker_client

    def docker_login(self, docker_client):
        client = self.get_client()
        response = client.get_authorization_token()

        if self.ecr_public_check:
            b64token = response['authorizationData']['authorizationToken']
            username, password = base64.b64decode(
                b64token).decode('utf-8').split(':')
            registry = self.public_registry
        else:
            b64token = response['authorizationData'][0]['authorizationToken']
            username, password = base64.b64decode(
                b64token).decode('utf-8').split(':')
            registry = response['authorizationData'][0]['proxyEndpoint']

        try:
            docker_client.login(username=username, password=password, registry=registry)
        except docker.errors.APIError as error:
            self.fail(f"Docker login error: {error}")

        logger.info(f'Successfully logged in to {registry}')

        return registry

    def check_image_tags_present(self, docker_client, image_name, tags):
        tags_not_found = []
        for tag in tags:
            try:
                docker_client.images.get(f"{image_name}:{tag}")
            except docker.errors.ImageNotFound:
                tags_not_found.append(f"{image_name}:{tag}")

        if tags_not_found:
            self.fail(f"Image not found error: {', '.join(tags_not_found)}.")

    def push(self, docker_client, image_name, tags, server_address):

        for tag in tags:
            self.tag_image(docker_client, image_name, tag, server_address)
            self.push_image(docker_client, image_name, tag, server_address)

    def tag_image(self, docker_client, image_name, tag, server_address):
        try:
            image = docker_client.images.get(f"{image_name}:{tag}")
        except docker.errors.ImageNotFound as error:
            self.fail(f"Image not found: {error}")

        try:
            result = image.tag(f"{server_address}/{image_name}", tag=tag)
        except docker.errors.APIError as error:
            self.fail(f"Docker tag error: {error}")

        if not result:
            self.fail(f"Failed to apply tag: {tag}")

    def push_image(self, docker_client, image_name, tag, server_address):
        for line in docker_client.images.push(
                f"{server_address}/{image_name}",
                tag,
                stream=True,
                decode=True
        ):
            self.log_info(line)

            if line.get('error') is not None:
                self.fail(
                    f"Docker push error: {line['errorDetail']['message']}")

    def run(self):
        super().run()

        self.log_info('Executing the aws-ecr-push-image pipe...')

        docker_client = self.get_docker_client()
        registry = self.docker_login(docker_client)

        if not self.ecr_public_check:
            server_address = registry.split('//')[1]
            image_name = self.get_variable('IMAGE_NAME')
        else:
            server_address = self.public_registry
            image_name = f"{self.public_registry_alias}/{self.get_variable('IMAGE_NAME')}"

        raw_tags = self.get_variable('TAGS').split()
        tags = [str(tag) for tag in raw_tags]

        self.check_image_tags_present(docker_client, image_name, tags)

        self.push(docker_client, image_name, tags, server_address)

        if not self.ecr_public_check:
            console = f"https://{self.region}.console.aws.amazon.com/ecr/repositories/{image_name}"

            self.success(
                message=f"Successfully pushed {image_name} to {registry}. \n"
                        f"The image is available in your ECR repository: {console}")
        else:
            self.success(message=f"Successfully pushed {image_name} to {registry}")


if __name__ == '__main__':
    metadata = yaml.safe_load(open('/pipe.yml', 'r'))
    pipe = ECRPush(pipe_metadata=metadata, schema=schema, logger=logger, check_for_newer_version=True)
    pipe.auth()
    pipe.run()
