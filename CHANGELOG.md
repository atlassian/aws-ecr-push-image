# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.4.2

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 2.4.1

- patch: Internal maintenance: Bump pipes versions in pipelines config file.
- patch: Internal maintenance: Update README example for ecr-public.

## 2.4.0

- minor: Update boto3 version to 1.34.*.
- patch: Internal maintenance: Update pipes versions in pipelines configuration file.
- patch: Internal maintenance: update requirements.
- patch: Refactor usage of ECR_PUBLIC variable to support false value if provided.

## 2.3.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 2.2.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 2.1.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.

## 2.0.0

- major: Breaking change: Refactored the logic of how pipe push images. Now you have to build images with all tags you want to push to ecr before pipe call.
- patch: Internal maintenance: Bump docker image, packages and pipe versions in pipelines configuration file, requirements and Dockerfile.

## 1.6.2

- patch: Internal maintenance: remove set_env.sh from repository.

## 1.6.1

- patch: Internal maintenance: update tests to check slashes usage.

## 1.6.0

- minor: Implement feature to support user's custom role arn.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update release process.

## 1.5.0

- minor: Added support of pushing images to Amazon Elastic Container Registry Public.
- minor: Bump version of boto3 to 1.20.* and bitbucket-pipes-toolkit to 3.2.1

## 1.4.2

- patch: Update readme: Add basic examples for tags. Fix variables description.

## 1.4.1

- patch: Fix ECR repository link

## 1.4.0

- minor: Ignore loading strings in YAML.

## 1.3.0

- minor: Support AWS OIDC authentication. Environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY are not required anymore.

## 1.2.2

- patch: Allow users to pass Ints into tag and then convert to String
- patch: Internal maintenance: Fix docker linter warnings.

## 1.2.1

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 1.2.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 1.1.3

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.1.2

- patch: Internal maintenance: Add gitignore secrets.

## 1.1.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 1.1.0

- minor: Add default values for AWS variables.

## 1.0.2

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 1.0.1

- patch: Internal maintenance: Add auto infrastructure for tests.

## 1.0.0

- major: Replaced the TAG with the TAGS parameter to support pushing multiple tags

## 0.2.0

- minor: Added warning messages when a new version of the pipe is available

## 0.1.3

- patch: Documentation improvements to provide more information about docker build and docker layers caching.
- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.1.2

- patch: Internal release

## 0.1.1

- patch: Bump the pipe patch version

## 0.1.0

- minor: Initial release
